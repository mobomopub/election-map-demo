
## Server instance
    
### - Required
- node
- mongodb


you can access to the server instance from the project root folder doing:
```
$ cd server
```

### Install (into server folder)

```javascript
$ npm install
```

### Run (into server folder)


```javascript
$ npm start
# or 
$ node server.js
```

### docker and docker-compose (into server folder)

```javascript
$ docker-compose up
```
just that! :)