const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('./../models/users');
const app = express();

app.post('/login', function (req, res) {

    let body = req.body;

    User.findOne({ email: body.email }, (erro, userDB)=>{
        if (erro) {
          return res.status(500).json({
             ok: false,
             err: erro
          })
       }
   // Verify that there is a user with the email written by the user.
      if (!userDB) {
         return res.status(400).json({
           ok: false,
           err: {
               message: "Wrong User or Password"
           }
        })
      }
   // Validate that the password written by the user is the one stored in the db
      if (! bcrypt.compareSync(body.password, userDB.password)){
         return res.status(400).json({
            ok: false,
            err: {
              message: "Wrong User or Password"
            }
         });
      }
   // Generate the authentication token
       let token = jwt.sign({
              user: userDB,
           }, process.env.SEED_AUTENTICACION, {
           expiresIn: process.env.CADUCIDAD_TOKEN
       })
       res.json({
           ok: true,
           user: userDB,
           token,
       })
   })
})

module.exports = app;