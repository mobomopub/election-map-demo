const express = require('express');
const bcrypt = require('bcrypt');
const User = require('./../models/users');
const app = express();

app.post('/register', function (req, res) {
    let body = req.body;
    let { name, email, password, role } = body;

    let user = new User({
        name,
        email,
        password: password!=null ? bcrypt.hashSync(password, 10) :null,
        role
    });
    user.save((err, userDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        res.json({
            ok: true,
            user: userDB
        });
    })
});

module.exports = app;