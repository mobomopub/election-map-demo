// ===========================
// port
// ===========================

process.env.PORT = process.env.PORT || 3001;

// ===========================
// environment
// ===========================

process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

// ===========================
// database
// ===========================

let urlDB = "mongodb://eletoralMapMongoDB:27017/ElectoralMap";

if (process.env.NODE_ENV === 'dev') {
    urlDB = "mongodb://eletoralMapMongoDB:27017/ElectoralMap";
} else {
    urlDB = "here write the mongo connection with mongo atlas and other type of connection mode"
};

process.env.URLDB = urlDB;


// ===========================
// token expiration
// ===========================

process.env.CADUCIDAD_TOKEN = '48h';

// ===========================
// autentication seed
// ===========================

process.env.SEED_AUTENTICACION = process.env.SEED_AUTENTICACION || 'this-is-the-development-seed';